--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.6
-- Dumped by pg_dump version 9.6.6

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: author; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE author (
    id integer NOT NULL,
    name character varying(255),
    family character varying(255),
    patronymic character varying(255),
    note character varying
);


ALTER TABLE author OWNER TO root;

--
-- Name: author_if_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE author_if_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE author_if_seq OWNER TO root;

--
-- Name: author_if_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE author_if_seq OWNED BY author.id;


--
-- Name: book; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE book (
    id integer NOT NULL,
    name character varying,
    genre_id integer NOT NULL,
    publishing_year integer,
    publishing_house character varying,
    number_of_volumes integer,
    painter character varying,
    provider_id integer NOT NULL,
    date_of_delivery date,
    date_of_sale date,
    purchase_price integer,
    selling_price integer,
    rack_number integer,
    shelf_number integer,
    series_name character varying,
    note character varying
);


ALTER TABLE book OWNER TO root;

--
-- Name: book_author; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE book_author (
    book_id integer NOT NULL,
    author_id integer NOT NULL
);


ALTER TABLE book_author OWNER TO root;

--
-- Name: book_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE book_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE book_id_seq OWNER TO root;

--
-- Name: book_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE book_id_seq OWNED BY book.id;


--
-- Name: client; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE client (
    id integer NOT NULL,
    name character varying(100),
    family character varying(100),
    patronymic character varying(100),
    telephone character varying(250),
    note character varying
);


ALTER TABLE client OWNER TO root;

--
-- Name: client_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE client_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE client_id_seq OWNER TO root;

--
-- Name: client_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE client_id_seq OWNED BY client.id;


--
-- Name: genre; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE genre (
    id integer NOT NULL,
    name character varying(100)
);


ALTER TABLE genre OWNER TO root;

--
-- Name: genre_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE genre_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE genre_id_seq OWNER TO root;

--
-- Name: genre_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE genre_id_seq OWNED BY genre.id;


--
-- Name: order; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE "order" (
    id integer NOT NULL,
    client_id integer,
    book_id integer
);


ALTER TABLE "order" OWNER TO root;

--
-- Name: order_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE order_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE order_id_seq OWNER TO root;

--
-- Name: order_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE order_id_seq OWNED BY "order".id;


--
-- Name: provider; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE provider (
    id integer NOT NULL,
    name character varying(150),
    family character varying(150),
    patronymic character varying(150),
    address character varying(300),
    passport character varying,
    note text
);


ALTER TABLE provider OWNER TO root;

--
-- Name: provider_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE provider_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE provider_id_seq OWNER TO root;

--
-- Name: provider_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE provider_id_seq OWNED BY provider.id;


--
-- Name: author id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY author ALTER COLUMN id SET DEFAULT nextval('author_if_seq'::regclass);


--
-- Name: book id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY book ALTER COLUMN id SET DEFAULT nextval('book_id_seq'::regclass);


--
-- Name: client id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY client ALTER COLUMN id SET DEFAULT nextval('client_id_seq'::regclass);


--
-- Name: genre id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY genre ALTER COLUMN id SET DEFAULT nextval('genre_id_seq'::regclass);


--
-- Name: order id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY "order" ALTER COLUMN id SET DEFAULT nextval('order_id_seq'::regclass);


--
-- Name: provider id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY provider ALTER COLUMN id SET DEFAULT nextval('provider_id_seq'::regclass);


--
-- Data for Name: author; Type: TABLE DATA; Schema: public; Owner: root
--

INSERT INTO author VALUES (2, 'Николай', 'Гоголь', 'Михайлович', '(фамилия при рождении Яновский, с 1821 года Гоголь-Яно́вский; 20 марта (1 апреля) 1809, Большие Сорочинцы, Полтавская губерния — 21 февраля (4 марта) 1852, Москва) — великий русский писатель, драматург, поэт., критик, публицист. ');
INSERT INTO author VALUES (3, 'Александр', 'Пушкин', 'Сергеевич', 'Поэт.(06.06.1799 - 10.02.1837) ');
INSERT INTO author VALUES (6, 'Уильям', 'Шекспир', 'Джонович', 'английский драматург и поэт, один из самых знаменитых драматургов мира.');
INSERT INTO author VALUES (7, 'Эрих-Мария', 'Ремарк', NULL, NULL);
INSERT INTO author VALUES (8, 'Лев', 'Толстой', NULL, NULL);
INSERT INTO author VALUES (9, 'Илья', 'Ильф', NULL, NULL);
INSERT INTO author VALUES (10, 'Евгений', 'Петров', NULL, NULL);
INSERT INTO author VALUES (1, 'Михаил ', 'Булгаков', 'Афанасьевич', 'русский писатель, драматург, театральный режиссёр и актёр. Автор повестей, рассказов, фельетонов, пьес, инсценировок, киносценариев и оперных либретт.');


--
-- Name: author_if_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('author_if_seq', 10, true);


--
-- Data for Name: book; Type: TABLE DATA; Schema: public; Owner: root
--

INSERT INTO book VALUES (46, 'Двенадцать стульев', 1, 1978, NULL, 2, NULL, 1, '2018-01-04', NULL, 160, 190, 2, 1, NULL, 'хорошее качество');
INSERT INTO book VALUES (7, 'Триумфальная арка ', 1, 2001, 'Вагриус ', 1, NULL, 1, NULL, NULL, 90, 120, 1, 1, NULL, '');
INSERT INTO book VALUES (31, 'Война и мир', 2, 2005, 'Вагриус ', 3, NULL, 2, '2018-10-24', NULL, 195, 250, 3, 1, 'Война и мир', 'Потрепанная');


--
-- Data for Name: book_author; Type: TABLE DATA; Schema: public; Owner: root
--

INSERT INTO book_author VALUES (7, 7);
INSERT INTO book_author VALUES (7, 2);
INSERT INTO book_author VALUES (7, 3);
INSERT INTO book_author VALUES (7, 6);
INSERT INTO book_author VALUES (31, 8);
INSERT INTO book_author VALUES (46, 9);
INSERT INTO book_author VALUES (46, 10);


--
-- Name: book_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('book_id_seq', 48, true);


--
-- Data for Name: client; Type: TABLE DATA; Schema: public; Owner: root
--

INSERT INTO client VALUES (1, 'Василий', 'Сюткин', 'Георгиевич', '37529-6355-655', 'Добросовестный. Во время возвращает взятое в долг. Должен мне еще за Экзюпери привезти денег. ');
INSERT INTO client VALUES (2, 'Николай', 'Барсуков', 'Иванович', '37529-877-990', 'Собачка "Ксюша" - болеет. Болонка. Любит Виски и Дюма. Угрюмый все время. ');
INSERT INTO client VALUES (3, 'Ольга', 'Семельникова', NULL, NULL, 'Все время навеселе. Разведена. Подсела на ром и Донцову. Оставляет сдачу постоянно. Любит говорить про "мужики - козлы". Не перебивать.');


--
-- Name: client_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('client_id_seq', 10, true);


--
-- Data for Name: genre; Type: TABLE DATA; Schema: public; Owner: root
--

INSERT INTO genre VALUES (1, 'Роман');
INSERT INTO genre VALUES (2, 'Повесть');
INSERT INTO genre VALUES (3, 'Рассказ');
INSERT INTO genre VALUES (4, 'Комедия');
INSERT INTO genre VALUES (5, 'Трагедия');
INSERT INTO genre VALUES (6, 'Драма');
INSERT INTO genre VALUES (7, 'Поэма');
INSERT INTO genre VALUES (8, 'Баллада');
INSERT INTO genre VALUES (9, 'Роман-эпопея');
INSERT INTO genre VALUES (10, 'Лирическое стихотворение');
INSERT INTO genre VALUES (11, 'Элегия');
INSERT INTO genre VALUES (12, 'Эпиграмма');
INSERT INTO genre VALUES (13, 'Ода');
INSERT INTO genre VALUES (14, 'Сонет');
INSERT INTO genre VALUES (15, 'сказка');
INSERT INTO genre VALUES (16, 'трагикомедия');


--
-- Name: genre_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('genre_id_seq', 16, true);


--
-- Data for Name: order; Type: TABLE DATA; Schema: public; Owner: root
--



--
-- Name: order_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('order_id_seq', 1, false);


--
-- Data for Name: provider; Type: TABLE DATA; Schema: public; Owner: root
--

INSERT INTO provider VALUES (2, 'Владимир ', 'Бойко', NULL, 'Якубова 30', 'МР 43434343434', NULL);
INSERT INTO provider VALUES (3, 'Андрей', 'Косяков', NULL, 'Плеханова 56', 'МР 52323454565', NULL);
INSERT INTO provider VALUES (1, 'John', 'Doe', '---', '---', '---', NULL);


--
-- Name: provider_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('provider_id_seq', 3, true);


--
-- Name: author author_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY author
    ADD CONSTRAINT author_pkey PRIMARY KEY (id);


--
-- Name: book_author book_author_pk; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY book_author
    ADD CONSTRAINT book_author_pk PRIMARY KEY (book_id, author_id);


--
-- Name: book book_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY book
    ADD CONSTRAINT book_pkey PRIMARY KEY (id);


--
-- Name: client client_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY client
    ADD CONSTRAINT client_pkey PRIMARY KEY (id);


--
-- Name: genre genre_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY genre
    ADD CONSTRAINT genre_pkey PRIMARY KEY (id);


--
-- Name: order order_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "order"
    ADD CONSTRAINT order_pkey PRIMARY KEY (id);


--
-- Name: provider provider_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY provider
    ADD CONSTRAINT provider_pkey PRIMARY KEY (id);


--
-- Name: book_author book_author_author_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY book_author
    ADD CONSTRAINT book_author_author_id_fk FOREIGN KEY (author_id) REFERENCES author(id);


--
-- Name: book_author book_author_book_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY book_author
    ADD CONSTRAINT book_author_book_id_fk FOREIGN KEY (book_id) REFERENCES book(id);


--
-- Name: book book_genre_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY book
    ADD CONSTRAINT book_genre_id_fk FOREIGN KEY (genre_id) REFERENCES genre(id);


--
-- Name: book book_provider_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY book
    ADD CONSTRAINT book_provider_id_fk FOREIGN KEY (provider_id) REFERENCES provider(id);


--
-- Name: order order_book_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "order"
    ADD CONSTRAINT order_book_id_fk FOREIGN KEY (book_id) REFERENCES book(id);


--
-- Name: order order_client_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY "order"
    ADD CONSTRAINT order_client_id_fk FOREIGN KEY (client_id) REFERENCES client(id);


--
-- PostgreSQL database dump complete
--

