package by.vinty.books._starter;

import by.vinty.books.repository.*;
import by.vinty.books.services.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        AuthorRepositoryTest.class,
        AuthorServiceImplTest.class,
        BookRepositoryTest.class,
        BookServiceImplTest.class,
        ClientRepositoryTest.class,
        ClientServiceImplTest.class,
        ProvidersRepositoryTest.class,
        ProvidersServiceImplTest.class,
        GenreRepositoryTest.class,
        GenreServiceImplTest.class
})
public class AtssertTestStarter {
}
