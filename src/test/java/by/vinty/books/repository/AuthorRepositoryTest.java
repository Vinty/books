package by.vinty.books.repository;

import by.vinty.books.config.CommonTest;
import by.vinty.books.entity.Author;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class AuthorRepositoryTest extends CommonTest {
    @Autowired
    private AuthorRepository authorRepository;

    public AuthorRepositoryTest() {

    }

    @Test
    public void findAllTest() {
        List<Author> authors = (List<Author>) authorRepository.findAllByOrderByFamilyAsc();
        Assert.assertTrue(authors.size() >0);
        System.out.println(authors);
    }

    @Test
    public void existsAuthorById() {
        List<Long> idBookAuthor = authorRepository.listIdBookAuthor();
        Long idFromDb = 0L;
        if (idBookAuthor.size() > 0) {
            idFromDb = Long.parseLong(String.valueOf((idBookAuthor.get(0))));
        }
        List<Long> longList = authorRepository.allAuthorIdByIdFromBookAuthorTable(idFromDb);
        Assert.assertTrue(longList.size() > 0);
    }

    @Test
    public void listIdBookAuthor() {
        List<Long> longList = authorRepository.listIdBookAuthor();
        Assert.assertTrue(longList.size() > 0);
    }

//    @Test
//    public void deleteTest() {
//        authorRepository.deleteById(29L);
//
//    }

//    @Test
//    public void findByFamilyIn() {
//        HashSet<Long> hashSet = new HashSet<>();
//        hashSet.add(2L);
//        hashSet.add(3L);
//        hashSet.add(7L);
//        hashSet.add(300L);
//
//        Iterable<Author> authors = authorRepository.findByIdIn(hashSet);
//        System.out.println(authors);
//    }
}