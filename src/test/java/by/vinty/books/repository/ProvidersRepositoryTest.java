package by.vinty.books.repository;

import by.vinty.books.config.CommonTest;
import by.vinty.books.entity.Providers;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class ProvidersRepositoryTest extends CommonTest {
    @Autowired
    private ProvidersRepository providersRepository;

    @Test
    public void findAllTest() {
        Iterable<Providers> providers = providersRepository.findAllByOrderByFamilyAsc();
        System.out.println(providers);
    }
}