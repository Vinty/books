package by.vinty.books.repository;

import by.vinty.books.config.CommonTest;
import by.vinty.books.entity.Client;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class ClientRepositoryTest extends CommonTest {
    @Autowired
    private ClientRepository clientRepository;

    @Test
    public void findAllByOrderByFamilyAsc() {
        Iterable<Client> clients = clientRepository.findAllByOrderByFamilyAsc();
        System.out.println(clients);
    }
}