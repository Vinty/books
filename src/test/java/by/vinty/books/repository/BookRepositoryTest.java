package by.vinty.books.repository;

import by.vinty.books.config.CommonTest;
import by.vinty.books.entity.Author;
import by.vinty.books.entity.Book;
import by.vinty.books.entity.Genre;
import by.vinty.books.entity.Providers;
import by.vinty.books.helpers.DateTimeHelper;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class BookRepositoryTest extends CommonTest {
    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private AuthorRepository authorRepository;

    @Test
    public void findAllTest() {
        Book book = new Book();
        Set<Author> authors = new HashSet<>();
        Author author = new Author();
        List<Author> authorRepositoryAll = (List<Author>) authorRepository.findAll();
        Author authorId = authorRepositoryAll.get(0);
        author.setId(authorId.getId());
        authors.add(author);
        book.setAuthor(authors);
        book.setName("Три Мушкетера");
        book.setGenreId(new Genre(1L));
        book.setProvidersId(new Providers(1L));
        book.setDateOfDelivery(DateTimeHelper.getLocalDate(Instant.now()));
        Assert.assertNotNull(book);
        Book savedBook = bookRepository.save(book);
        List<Book> all = (List<Book>) bookRepository.findAll();
        Assert.assertNotNull(all);
        List<Book> bookList = bookRepository.findAllById(savedBook.getId());
        Assert.assertNotNull(bookList);
        System.out.println(bookList);
        bookRepository.delete(savedBook);
    }

    @Test
    public void findAllByNameTest() {
        Book book = new Book();
        Set<Author> authors = new HashSet<>();
        Author author = new Author();
        List<Author> authorRepositoryAll = (List<Author>) authorRepository.findAll();
        Author authorId = authorRepositoryAll.get(0);
        author.setId(authorId.getId());
        authors.add(author);
        book.setAuthor(authors);
        book.setName("Три Мушкетера");
        book.setGenreId(new Genre(1L));
        book.setProvidersId(new Providers(1L));
        book.setPurchasePrice(290.34);
        bookRepository.save(book);
        List<Book> all = (List<Book>) bookRepository.findAllByNameContainsIgnoreCase("или 22334455");
        System.out.println(all);
        Assert.assertNotNull(all);
        bookRepository.delete(book);
    }

    @Test
    public void findAllByNameContainsOrderByAuthor() {

    }
}