package by.vinty.books.repository;

import by.vinty.books.config.CommonTest;
import by.vinty.books.entity.Genre;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class GenreRepositoryTest extends CommonTest {

    @Autowired
    private GenreRepository genreRepository;

    @Test
    public void findAllByOrderByNameAsc() {
        Genre genre = genreRepository.save(new Genre());
        Iterable<Genre> allByOrderByNameAsc = genreRepository.findAllByOrderByNameAsc();
        allByOrderByNameAsc.forEach(Assert::assertNotNull);
        genreRepository.delete(genre);
    }
}