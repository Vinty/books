package by.vinty.books.services;

import by.vinty.books.config.CommonTest;
import by.vinty.books.entity.Client;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

public class ClientServiceImplTest extends CommonTest {
    @Autowired
    private ClientService clientService;

    @Test
    public void findAll() {
        Client client = new Client();
        client.setName("VV22BB");
        Long id = clientService.save(client);
        Assert.assertNotNull(id);
        Iterable<Client> clients = this.clientService.findAll();
        Assert.assertNotNull(clients);
        clientService.delete(id);
    }

    @Test
    public void findById() {
        Client client = new Client();
        client.setName("VV22BB");
        Long id = clientService.save(client);
        Assert.assertNotNull(id);
        Optional<Client> optionalClient = clientService.findById(id);
        boolean result = false;
        if (optionalClient.isPresent()) {
            result = true;
        }
        Assert.assertTrue(result);
        if (result) {
            clientService.delete(optionalClient.get().getId());
        }
    }

    @Test
    public void findByClient() {
        Client client = new Client();
        client.setName("Сергей");
        client.setFamily("Волков");
        Long id = clientService.save(client);
        Assert.assertNotNull(id);
        List<Client> clientList = clientService.findByClient("олк");
        Assert.assertNotNull(clientList);
        clientService.delete(id);
    }
}