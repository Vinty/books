package by.vinty.books.services;

import by.vinty.books.config.CommonTest;
import by.vinty.books.entity.Providers;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class ProvidersServiceImplTest extends CommonTest {

    @Autowired
    private ProvidersService providersService;

    @Test
    public void findAll() {
        Iterable<Providers> providers = providersService.findAll();
        providers.forEach(Assert::assertNotNull);
    }
}