package by.vinty.books.services;

import by.vinty.books.config.CommonTest;
import by.vinty.books.entity.Genre;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class GenreServiceImplTest extends CommonTest {

    @Autowired
    public GenreService genreService;

    @Test
    public void findAll() {
        Iterable<Genre> genres = genreService.findAll();
        genres.forEach(Assert::assertNotNull);
    }
}