package by.vinty.books.services;

import by.vinty.books.config.CommonTest;
import by.vinty.books.entity.Author;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

public class AuthorServiceImplTest extends CommonTest {

    @Autowired
    private AuthorService authorService;

    @Test
    public void findAllTest() {
        Iterable<Author> authors = authorService.findAll();
        System.out.println(authors);
        Assert.assertNotNull(authors);
    }

    @Test
    public void save() {
        Author author = new Author();
        author.setName("Vinty");
        author.setFamily("Waw");
        Long id = authorService.save(author);
        Assert.assertNotNull(id);
        Optional<Author> authorOptional = authorService.findById(id);
        Author authorFromDB = null;
        if (authorOptional.isPresent()) {
            authorFromDB = authorOptional.get();
        }
        Assert.assertEquals(author.getName(), authorFromDB.getName());
        authorService.delete(id);
    }

    @Test
    public void delete() {
        Author author = new Author();
        author.setName("Vinty");
        author.setFamily("Waw");
        Long id = authorService.save(author);
        Assert.assertNotNull(id);
        authorService.delete(id);
        Optional<Author> authorOptional = authorService.findById(id);
        boolean result = true;
        if (authorOptional.isPresent()) {
            result = false;
        }
        Assert.assertTrue(result);
    }

    @Test
    public void isExistAuthorIdFromBookAuthor() {
        List<Long> authors = authorService.listIdBookAuthor();
        Long id = null;
        boolean author = false;
        if (authors.size() > 0) {
            id = Long.parseLong(String.valueOf((authors.get(0))));
            author = authorService.isExistAuthorIdFromBookAuthor(id);
        }
        Assert.assertTrue(author);
    }
}
