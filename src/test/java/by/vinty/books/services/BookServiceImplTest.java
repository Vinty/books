package by.vinty.books.services;

import by.vinty.books.config.CommonTest;
import by.vinty.books.entity.Author;
import by.vinty.books.entity.Book;
import by.vinty.books.entity.Genre;
import by.vinty.books.entity.Providers;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class BookServiceImplTest extends CommonTest {

    @Autowired
    private BookService bookService;

    @Autowired
    private AuthorService authorService;

    @Test
    public void findAllTest() {
        List<Book> all = bookService.findAll();
        Assert.assertNotNull(all);
    }

    @Test
    public void findByIdTest() {
        Book book = new Book();
        Set<Author> authors = new HashSet<>();
        List<Author> list = (List<Author>) authorService.findAll();
        Author author = list.get(0);
        authors.add(author);
        book.setAuthor(authors);
        book.setName("Три Мушкетера");
        book.setGenreId(new Genre(1L));
        book.setProvidersId(new Providers(1L));
        bookService.save(book);
        Assert.assertNotNull(book);
        List<Book> all = bookService.findAll();
        Book b = null;
        if (all.size() > 0) {
            b = all.get(0);
        }
        Optional<Book> bookOptional = bookService.findById(b.getId());
        Assert.assertTrue(bookOptional.isPresent());
        bookService.delete(book);
    }

    @Test
    public void findAllByNameContainsTest() {
        Book b = new Book();
        b.setName("Три Мушкетера");
        b.setGenreId(new Genre(1L));
        b.setProvidersId(new Providers(1L));
        Book b2 = new Book();
        b2.setName("Три или 22334455 Два Мушкетера");
        b2.setGenreId(new Genre(1L));
        b2.setProvidersId(new Providers(1L));
        bookService.save(b);
        bookService.save(b2);
        List<Book> all = (List<Book>) bookService.findAllByNameContains("или 223");
        Book o = null;
        if (all.size() > 0) {
            o = all.get(0);
        }
        Optional<Book> bookOptional = bookService.findById(o.getId());
        Assert.assertTrue(bookOptional.isPresent());
        System.out.println(bookOptional.get());
        bookService.delete(b);
        bookService.delete(b2);
    }
}