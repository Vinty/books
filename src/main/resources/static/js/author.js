"use strict";

// формирование универсального url = http://localhost:8080/
const fullUrl = window.location.href;
const url = window.location.href.replace(fullUrl.split('/').splice(-1, 1).join('/'), '');

function addAuthor() {
    const frm1 = document.forms.form1;
    swal.mixin({
        input: 'text',
        confirmButtonText: 'Далее &rarr;',
        showCancelButton: true,
        progressSteps: ['1', '2', '3', '4']
    }).queue([
        {
            title: 'Имя ',
            text: 'Заполняем данные на Автора'
        },
        'Фамилия',
        'Отчество',
        'Краткое описание'
    ]).then(async (result) => {
        if (result.value) {
            try {
                const dataForREST = await fetch(url + "api/addAuthor",
                    {
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        method: "POST",
                        body: JSON.stringify({myArray: result.value})
                    }).then(res => {
                    if (res.status === 200) {
                        swal({
                            title: 'Сохранили!',
                            html:
                                'Ваш набор данных: <pre><code>' +
                                JSON.stringify(result.value) +
                                '</code></pre>',
                            confirmButtonText: 'Продолжить'
                        }).then(() => {
                                // const element = document.createElement('a');
                                // element.setAttribute('href', `${url}authors`);
                                // element.click();
                            window.location.href = `${url}authors`;
                            }
                        )
                        ;
                        return true;
                    }
                    throw new Error()
                })
            }
            catch (e) {
                console.log(e);
            }
        }
    });
}

// async function addAuthor2() {
//     const {value: formValues} = await swal({
//         title: 'Multiple inputs',
//         html:
//             '<input id="swal-input1" class="swal2-input">' +
//             '<input id="swal-input2" class="swal2-input">',
//         focusConfirm: false,
//         preConfirm: () => {
//             return [
//                 document.getElementById('swal-input1').value,
//                 document.getElementById('swal-input2').value
//             ]
//         }
//     })
//     if (formValues) {
//         swal(json.stringify(formValues))
//     }
// }


// document.addEventListener("DOMContentLoaded", () => {
//     console.log("in begin");
//     const frm1 = document.forms.form1;
//     frm1.onsubmit = (e) => {
//         e.preventDefault();
//         console.log("in onsubmit");
//         swal("Добавление автора", "Заполните форму", "success")
//         // https://sweetalert.js.org/guides/
//             .then(() => frm1.submit());
//     }
// });