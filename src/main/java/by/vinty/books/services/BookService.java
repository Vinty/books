package by.vinty.books.services;

import by.vinty.books.entity.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface BookService {

    Page<Book> findAllByPage(Pageable pageable);

    List<Book> findAll();

    List<Book> findAllByNameContains(String bookName);

    void save(Book book);

    void delete(Book book);

    Optional<Book> findById(Long id);

}
