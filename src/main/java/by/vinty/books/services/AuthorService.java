package by.vinty.books.services;

import by.vinty.books.entity.Author;

import java.util.List;
import java.util.Optional;

public interface AuthorService {

    Iterable<Author> findAll();

    Long save(Author author);

    Optional<Author> findById(Long id);

    void delete(Long id);

    void delete(Author author);

    boolean isExistAuthorIdFromBookAuthor(Long id);

    List<Long> listIdBookAuthor();

}
