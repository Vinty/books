package by.vinty.books.services;

import by.vinty.books.entity.Genre;

public interface GenreService {

    Iterable<Genre> findAll();

}
