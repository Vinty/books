package by.vinty.books.services;

import by.vinty.books.entity.Client;
import by.vinty.books.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;

    @Autowired
    public ClientServiceImpl(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Override
    public Iterable<Client> findAll() {
        return clientRepository.findAllByOrderByFamilyAsc();
    }

    @Override
    public Long save(Client client) {
        Client id = clientRepository.save(client);
        return id.getId();
    }

    @Override
    public Optional<Client> findById(Long id) {
        return clientRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        clientRepository.deleteById(id);
    }

    @Override
    public List<Client> findByClient(String family) {
        return (List<Client>) clientRepository.findAllByFamilyContaining(family);
    }
}
