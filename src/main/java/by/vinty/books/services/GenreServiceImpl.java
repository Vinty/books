package by.vinty.books.services;

import by.vinty.books.entity.Author;
import by.vinty.books.entity.Genre;
import by.vinty.books.repository.AuthorRepository;
import by.vinty.books.repository.GenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class GenreServiceImpl implements GenreService {

    private final GenreRepository genreRepository;

    @Autowired
    public GenreServiceImpl(GenreRepository genreRepository) {
        this.genreRepository = genreRepository;
    }

    @Override
    public Iterable<Genre> findAll() {
        return genreRepository.findAllByOrderByNameAsc();
    }
}
