package by.vinty.books.services;

import by.vinty.books.entity.Providers;

public interface ProvidersService {

    Iterable<Providers> findAll();

}
