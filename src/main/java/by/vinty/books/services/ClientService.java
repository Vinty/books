package by.vinty.books.services;

import by.vinty.books.entity.Client;

import java.util.List;
import java.util.Optional;

public interface ClientService {

    Iterable<Client> findAll();

    Long save(Client client);

    Optional<Client> findById(Long id);

    void delete(Long id);

    List<Client> findByClient(String family);

}
