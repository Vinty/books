package by.vinty.books.services;

import by.vinty.books.entity.Providers;
import by.vinty.books.repository.ProvidersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class ProvidersServiceImpl implements ProvidersService {

    private final ProvidersRepository providersRepository;

    @Autowired
    public ProvidersServiceImpl(ProvidersRepository providersRepository) {
        this.providersRepository = providersRepository;
    }

    @Override
    public Iterable<Providers> findAll() {
        return providersRepository.findAllByOrderByFamilyAsc();
    }
}
