package by.vinty.books.services;

import by.vinty.books.aspects.TrackTime;
import by.vinty.books.entity.Author;
import by.vinty.books.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AuthorServiceImpl implements AuthorService {

    @Autowired
    private AuthorRepository authorRepository;

    @Override
    public Iterable<Author> findAll() {
        return authorRepository.findAllByOrderByFamilyAsc();
    }

    @Override
    public Long save(Author author) {
        Author author1 = authorRepository.save(author);
        return author1.getId();
    }

    @Override
    public Optional<Author> findById(Long id) {
        return authorRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        authorRepository.deleteById(id);
    }

    @Override
    public void delete(Author author) {
        authorRepository.delete(author);
    }

    @Override
    public boolean isExistAuthorIdFromBookAuthor(Long id) {
        List<Long> longList = authorRepository.allAuthorIdByIdFromBookAuthorTable(id);
         return longList.size() > 0;
    }

    @Override
    public List<Long> listIdBookAuthor() {
        return authorRepository.listIdBookAuthor();
    }


}
