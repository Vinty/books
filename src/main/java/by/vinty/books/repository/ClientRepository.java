package by.vinty.books.repository;

import by.vinty.books.entity.Client;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface ClientRepository extends CrudRepository<Client, Long> {

    Iterable<Client> findAllByOrderByFamilyAsc();

    Iterable<Client> findAllByFamilyContaining(String family);



}
