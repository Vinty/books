package by.vinty.books.repository;

import by.vinty.books.entity.Author;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public interface AuthorRepository extends CrudRepository<Author, Long> {

    Iterable<Author> findAllByOrderByFamilyAsc();

    @Query(value = "SELECT 1 as result from book_author b where b.author_id = ?1 limit 1", nativeQuery = true)
    List<Long> allAuthorIdByIdFromBookAuthorTable(Long aLong);

    @Query(value = "SELECT distinct (b.author_id) as ids from book_author b", nativeQuery = true)
    List<Long> listIdBookAuthor();

}
