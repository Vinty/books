package by.vinty.books.repository;

import by.vinty.books.entity.Providers;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface ProvidersRepository extends CrudRepository<Providers, Long> {

    Iterable<Providers> findAllByOrderByFamilyAsc();

}
