package by.vinty.books.repository;

import by.vinty.books.entity.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;

@Transactional
@Repository
public interface BookRepository extends PagingAndSortingRepository<Book, Long> {

    List<Book> findAllById(Long id);

    List<Book> findAllByNameContainsIgnoreCase(String bookName);

    List<Book> findAllByNameContainsOrderByAuthor(String bookName);

    Iterable<Book> findByIdIn(Set<Long> myField);

    Page<Book> findAllByCountGreaterThan(Pageable pageable, Long grtThenZero);

    List<Book> findAllByCountEquals(Long zero);

}
