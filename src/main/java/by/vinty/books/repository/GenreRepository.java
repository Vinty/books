package by.vinty.books.repository;

import by.vinty.books.entity.Genre;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface GenreRepository extends CrudRepository<Genre, Long> {

    Iterable<Genre> findAllByOrderByNameAsc();

}
