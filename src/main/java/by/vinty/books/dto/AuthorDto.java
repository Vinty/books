package by.vinty.books.dto;

import lombok.*;

@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Setter
public class AuthorDto {
    private String[] myArray;
}
