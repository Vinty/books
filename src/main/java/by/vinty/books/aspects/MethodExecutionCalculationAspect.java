package by.vinty.books.aspects;

import by.vinty.books.BooksApplication;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@Aspect
@Configuration
public class MethodExecutionCalculationAspect {
    private static final Logger logger = LogManager.getLogger(BooksApplication.class);

    public MethodExecutionCalculationAspect() throws IOException {
    }
//        private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Around("@annotation(by.vinty.books.aspects.TrackTime)")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();

        Object result = joinPoint.proceed();

        long timeTaken = System.currentTimeMillis() - startTime;
        logger.warn("timerExecuteMethod {} = {}", joinPoint.getSignature().getName(), timeTaken);
        return result;
    }
}
