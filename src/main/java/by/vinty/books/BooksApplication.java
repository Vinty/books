package by.vinty.books;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.charset.Charset;

@SpringBootApplication
@PropertySource(ignoreResourceNotFound = true, value = "file:${MY_APP_HOME}/application.properties")
@PropertySource("classpath:/application.properties")
@PropertySource(ignoreResourceNotFound = true, value = "file:${user.home}/.myapp/application.properties")
public class BooksApplication {

    public static void main(String[] args) throws IOException {
        System.setProperty("file.encoding","UTF-8");
        Field charset = null;
        try {
            charset = Charset.class.getDeclaredField("defaultCharset");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        charset.setAccessible(true);
        try {
            charset.set(null,null);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        SpringApplication.run(BooksApplication.class, args);
    }
}
