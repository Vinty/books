package by.vinty.books.controllers;

import by.vinty.books.BooksApplication;
import by.vinty.books.aspects.TrackTime;
import by.vinty.books.entity.Book;
import by.vinty.books.services.BookService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
public class BookController {
    private static int currentPage = 1;
    private static int pageSize = 5;
    private static final Logger logger = LogManager.getLogger(BooksApplication.class);

    private final BookService bookService;

    @Autowired
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @TrackTime
    @RequestMapping(value = "/books", method = RequestMethod.GET)
    public String listBooks(
            Model model,
            @RequestParam("page") Optional<Integer> page,
            @RequestParam("size") Optional<Integer> size) {
        page.ifPresent(p -> currentPage = p);
        size.ifPresent(s -> pageSize = s);
        Page<Book> bookPage = bookService.findAllByPage(PageRequest.of(currentPage - 1, pageSize));
        model.addAttribute("bookPage", bookPage);
        int totalPages = bookPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }
        return "books";
    }

//    @TrackTime
//    @ModelAttribute("myBooks")
//    public Iterable<Book> listAllBooks() {
//        return bookService.findAll();
//    }

//    @TrackTime
//    @ModelAttribute("myBooks")
//    public Iterable<Book> listAllBooks() {
//        return bookService.findAllByPage();
//    }

//    @GetMapping(path = "/books")
//    public ModelAndView showBooks() {
//        return new ModelAndView("books");
//    }

    @TrackTime
    @GetMapping(path = "/book/delete/{id}")
    public String deleteBook(@PathVariable String id) {
        Optional<Book> bookForLoging = bookService.findById(Long.valueOf(id));
        Book book = new Book();
        book.setId(Long.valueOf(id));
        logger.info("Книга удалена. " + bookForLoging.get());
        bookService.delete(book);
        return "redirect:/books";
    }

    @TrackTime
    @GetMapping(path = "/book/sell/{id}")
    public String sellBook(@PathVariable String id) {
        Book book = new Book();
        book.setId(Long.valueOf(id));
        Optional<Book> bookOptional = bookService.findById(book.getId());
        if (bookOptional.isPresent()) {
            Book bookFromDb = bookOptional.get();
            Optional<Long> countBook = Optional.ofNullable(bookFromDb.getCount());
            if ((countBook.isPresent()) && (countBook.get() > 0)) {
                bookFromDb.setCount(countBook.get() - 1);
                bookService.save(bookFromDb);
                logger.info("Книга изменена. Ее пометили как 'продана'." + bookFromDb);
            }
        } else {
            logger.info("В БД запросили книгу для продажи, но найти ее по Id = " + id + " не удалось.");
        }
        return "redirect:/books";
    }

    @TrackTime
    @GetMapping(path = "/book/plusOne/{id}")
    public String plusBook(@PathVariable String id) {
        Book book = new Book();
        book.setId(Long.valueOf(id));
        Optional<Book> bookOptional = bookService.findById(book.getId());
        if (bookOptional.isPresent()) {
            Book bookFromDb = bookOptional.get();
            Optional<Long> countBook = Optional.ofNullable(bookFromDb.getCount());
            if ((countBook.isPresent())) {
                bookFromDb.setCount(countBook.get() + 1);
                bookService.save(bookFromDb);
                logger.info("Количество книг увеличено на 1.");
            }
        } else {
            logger.info("В БД запросили книгу для добавления, но найти ее по Id = " + id + " не удалось.");
        }
        return "redirect:/books";
    }

    @PostMapping(path = "/books")
    public ModelAndView search(@RequestParam String searchBook) {
        ModelAndView modelAndView = new ModelAndView("books");
        List<Book> bookList = bookService.findAllByNameContains(searchBook);
        modelAndView.addObject("myBooks", bookList);
        return modelAndView;
    }
}
