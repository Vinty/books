package by.vinty.books.controllers;

import by.vinty.books.aspects.TrackTime;
import by.vinty.books.entity.Author;
import by.vinty.books.entity.Book;
import by.vinty.books.entity.Genre;
import by.vinty.books.entity.Providers;
import by.vinty.books.services.AuthorService;
import by.vinty.books.services.BookService;
import by.vinty.books.services.GenreService;
import by.vinty.books.services.ProvidersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
public class AddBookController {

    private final BookService bookService;
    private final GenreService genreService;
    private final ProvidersService providersService;
    private final AuthorService authorService;

    @Autowired
    public AddBookController(
            BookService bookService,
            GenreService genreService,
            ProvidersService providersService,
            AuthorService authorService) {
        this.bookService = bookService;
        this.genreService = genreService;
        this.providersService = providersService;
        this.authorService = authorService;
    }

    @ModelAttribute("book")
    public Book book() {
        return new Book();
    }

    @ModelAttribute("providersId")
    public Providers getProvidersId() {
        return new Providers();
    }

    @ModelAttribute("author")
    public Set<Author> getAuthor() {
        return new HashSet<>();
    }

    @ModelAttribute("authors")
    public List<Author> getAutors() {
        return (List<Author>) authorService.findAll();
    }

    @ModelAttribute("providers")
    public List<Providers> getProviders() {
        return (List<Providers>) providersService.findAll();
    }

    @ModelAttribute("genres")
    public List<Genre> genreList() {
        return (List<Genre>) genreService.findAll();
    }

    @GetMapping(path = "/addBook")
    public String showBooks() {
        return "addBook";
    }


    //    @PostMapping(path = "/addBook/save")
//    public ModelAndView saveBook(@ModelAttribute Book book) {
//        bookService.save(book);
//        ModelAndView mav = new ModelAndView("/addBook");
//        mav.addObject("book", new Book());
//        mav.addObject("providersId", new Providers());
//        mav.addObject("author", new HashSet<>());
//        mav.addObject("authors", (List<Author>) authorService.findAll());
//        mav.addObject("providers", (List<Providers>) providersService.findAll());
//        mav.addObject("genres", (List<Genre>) genreService.findAll());
//        return mav;
//    }

    @TrackTime
    @PostMapping(path = "/addBook/save")
    public String saveBook(Book book) {
        book.setCount(1L);
        bookService.save(book);
        return "redirect:/addBook";
    }
}
