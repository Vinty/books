package by.vinty.books.controllers;

import by.vinty.books.entity.Client;
import by.vinty.books.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ClientController {

    private final ClientService clientService;

    @Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @ModelAttribute("myClients")
    public Iterable<Client> listAllClientsOrderByFamily() {
        return clientService.findAll();
    }

    @RequestMapping(value = "/clients", produces = "text/plain;charset=UTF-8", method = RequestMethod.GET)
    public ModelAndView showPage() {
        return new ModelAndView("clients");
    }
}
