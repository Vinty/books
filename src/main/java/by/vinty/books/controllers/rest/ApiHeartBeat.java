package by.vinty.books.controllers.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApiHeartBeat {

    @GetMapping("/api/heartBeat")
    public String getHeatbeat() {
        return "Heartbeat beats great!";
    }

}
