package by.vinty.books.controllers.rest;

import by.vinty.books.BooksApplication;
import by.vinty.books.aspects.TrackTime;
import by.vinty.books.dto.AuthorDto;
import by.vinty.books.entity.Author;
import by.vinty.books.services.AuthorService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class ApiAuthorController {
    private static final Logger logger = LogManager.getLogger(BooksApplication.class);

    private final AuthorService authorService;

    @Autowired
    public ApiAuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @TrackTime
    @GetMapping("/api/getAuthors")
    public List<Author> getAuthors() {
        return (List<Author>) authorService.findAll();
    }

    @TrackTime
    @GetMapping("/api/getAuthor/{id}")
    @ResponseBody
    public Author showJson(@PathVariable Long id) {
        Optional<Author> authorFromDb = authorService.findById(id);
        if (!authorFromDb.isPresent()) {
            logger.warn("Такого автора (id=) " + id + " не существует!");
        }
        return authorFromDb.orElse(null);
    }

    @GetMapping("/api/getAuthorDto/{id}")
    @ResponseBody
    public AuthorDto showJsonDto(@PathVariable Long id) {
        Optional<Author> authorFromDb = authorService.findById(id);
        AuthorDto dto = new AuthorDto();
        String[] myArray = new String[4];
        if (authorFromDb.isPresent()) {
            Author author = authorFromDb.get();
            myArray[0] = author.getName();
            myArray[1] = author.getFamily();
            myArray[2] = author.getPatronymic();
            myArray[3] = author.getNote();
            dto.setMyArray(myArray);
        }
        return dto;
    }
}
