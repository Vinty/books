package by.vinty.books.controllers.rest;

import by.vinty.books.BooksApplication;
import by.vinty.books.aspects.TrackTime;
import by.vinty.books.entity.Book;
import by.vinty.books.services.BookService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApiBookController {

    private final BookService bookService;

    @Autowired
    public ApiBookController(BookService bookService) {
        this.bookService = bookService;
    }

    @TrackTime
    @GetMapping("/api/books")
    public Page<Book> getBook(Pageable pageable) {
        return bookService.findAllByPage(pageable);
    }
}
