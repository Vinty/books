package by.vinty.books.controllers.rest;

import by.vinty.books.BooksApplication;
import by.vinty.books.aspects.TrackTime;
import by.vinty.books.dto.AuthorDto;
import by.vinty.books.entity.Author;
import by.vinty.books.helpers.DateTimeHelper;
import by.vinty.books.services.AuthorService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.time.LocalDate;

@RestController
public class AddAuthorController {
    private static final Logger logger = LogManager.getLogger(BooksApplication.class);

    private final AuthorService authorService;

    @Autowired
    public AddAuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @TrackTime
    @PostMapping(value = "/api/addAuthor", consumes = "application/json", produces = "application/json")
    public Author addAuthor(@RequestBody AuthorDto authorDto) throws IOException {
//        long startTimer = System.currentTimeMillis();
        Author author = new Author();
        author.setName(authorDto.getMyArray()[0]);
        author.setFamily(authorDto.getMyArray()[1]);
        author.setPatronymic(authorDto.getMyArray()[2]);
        author.setNote(authorDto.getMyArray()[3]);
        System.out.println(author);
        authorService.save(author);
        logger.info("Пользователь создан!" + author);
//        logger.warn("microTimer addAuthor() " + (System.currentTimeMillis() - startTimer));
        return author;
    }
}
