package by.vinty.books.controllers;

import by.vinty.books.entity.Providers;
import by.vinty.books.services.ProvidersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@Controller
public class ProvidersController {

    private final ProvidersService providersService;

    @Autowired
    public ProvidersController(ProvidersService providersService) {
        this.providersService = providersService;
    }

    @ModelAttribute("providers")
    public Iterable<Providers> listAllProvider() {
        return providersService.findAll();
    }

    @RequestMapping(value = "/providers", produces = "text/plain;charset=UTF-8", method = RequestMethod.GET)
    public ModelAndView showPage(HttpSession httpSession) {
        return new ModelAndView("providers");
    }
}
