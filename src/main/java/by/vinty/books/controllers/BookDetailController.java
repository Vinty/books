package by.vinty.books.controllers;

import by.vinty.books.entity.Book;
import by.vinty.books.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;

@Controller
public class BookDetailController {

    private BookService bookService;

    @Autowired
    public BookDetailController(BookService bookService) {
        this.bookService = bookService;
    }

    @RequestMapping(value = "/bookDetail/{id}", method = RequestMethod.GET)
    public ModelAndView findBookById(@PathVariable Long id) {
        ModelAndView modelAndView = new ModelAndView("bookDetail");
        Optional<Book> book = this.bookService.findById(id);
        System.out.println(book);
        modelAndView.addObject("book", book.get());
        return modelAndView;
    }
}
