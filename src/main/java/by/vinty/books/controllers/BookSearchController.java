package by.vinty.books.controllers;

import by.vinty.books.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@Controller
public class BookSearchController {

    private final BookRepository bookRepository;

    @Autowired
    public BookSearchController(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @RequestMapping(value = "/books/{searchBook}", produces = "text/plain;charset=UTF-8", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView searchByBook(HttpSession httpSession, @PathVariable String searchBook) {
        ModelAndView modelAndView = new ModelAndView("books");
        modelAndView.addObject("myBooks", bookRepository.findAllByNameContainsIgnoreCase(searchBook.toLowerCase()));
        return modelAndView;
    }
}
