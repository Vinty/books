package by.vinty.books.controllers;

import by.vinty.books.BooksApplication;
import by.vinty.books.aspects.TrackTime;
import by.vinty.books.entity.Author;
import by.vinty.books.services.AuthorService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class AuthorController {
    private static final Logger logger = LogManager.getLogger(BooksApplication.class);

    private final AuthorService authorService;

    @Autowired
    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }


    @ModelAttribute("myAuthors")
    public Iterable<Author> listAllAuthors() {
        return authorService.findAll();
    }

    @ModelAttribute("authorsIds")
    public List<Long> authorsIds() {
        return authorService.listIdBookAuthor();
    }

    @TrackTime
    @RequestMapping(value = "/authors", produces = "text/plain;charset=UTF-8", method = RequestMethod.GET)
    public ModelAndView showPage() {
        return new ModelAndView("authors");
    }

    @TrackTime
    @GetMapping(path = "/author/delete/{id}", produces = "text/html; charset=utf-8")
    public String deleteAuthor(@PathVariable String id) {
        if (!authorService.isExistAuthorIdFromBookAuthor(Long.valueOf(id))) {
            authorService.delete(Long.valueOf(id));
        }
        return "redirect:/authors";
    }
}
