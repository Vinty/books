package by.vinty.books.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "provider")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Providers extends BaseEntity {
    private String name;
    private String family;
    private String patronymic;
    private String address;
    private String passport;
    private String note;

    public Providers(long id) {
        super(id);
    }
}
