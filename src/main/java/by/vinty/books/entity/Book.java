package by.vinty.books.entity;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "book")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Book extends BaseEntity {
    private String name;
    @ManyToOne
    @JoinColumn(name = "genre_id")
    private Genre genreId;
    private Long publishingYear;
    private String publishingHouse;
    private Long numberOfVolumes;
    private String painter;
    @ManyToOne
    @JoinColumn(name = "provider_id")
    private Providers providersId;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private LocalDate dateOfDelivery;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private LocalDate dateOfSale;
    private Double purchasePrice;
    private Double sellingPrice;
    private Long rackNumber;
    private Long shelfNumber;
    private String seriesName;
    private String note;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "book_author",
            joinColumns = @JoinColumn(name = "book_id"),
            inverseJoinColumns = @JoinColumn(name = "author_id"))
    private Set<Author> author = new HashSet<>();
    private Long count;

    public Book(Long id) {
        super(id);
    }
}
