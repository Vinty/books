package by.vinty.books.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "client")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class Client extends BaseEntity{
    private String name;
    private String family;
    private String patronymic;
    private String telephone;
    private String note;
}
