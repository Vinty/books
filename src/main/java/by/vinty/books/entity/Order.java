package by.vinty.books.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "order")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Order extends BaseEntity{
    @ManyToOne
    @JoinColumn(name = "client_id")
    private Client clientId;
    @ManyToOne
    @JoinColumn(name = "book_id")
    private Book bookId;
}
