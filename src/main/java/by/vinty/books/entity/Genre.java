package by.vinty.books.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "genre")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Genre extends BaseEntity{
    private String name;

    public Genre(Long id) {
        super(id);
    }
}
