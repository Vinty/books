package by.vinty.books.entity;

import lombok.*;
import org.springframework.context.annotation.Bean;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "author")
@Getter
@Setter
@NoArgsConstructor
public class Author extends BaseEntity {
    private String name;
    private String family;
    private String patronymic;
    private String note;

    public Author(String name, String family, String patronymic, String note) {
        this.name = name;
        this.family = family;
        this.patronymic = patronymic;
        this.note = note;
    }

    @Override
    public String toString() {
        return  name + " " + family;
    }
}
